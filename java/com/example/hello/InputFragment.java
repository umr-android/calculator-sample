package com.example.hello;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.hello.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class InputFragment extends Fragment {

    private ListView listView;
    private CustomAdapter adapter;
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);
        listView = view.findViewById(R.id.listView);

        databaseHelper = new DatabaseHelper(getActivity());
        database = databaseHelper.getWritableDatabase();

        // Query the database to get all values from the calculation_history table
        Cursor cursor = database.query("calculation_history", null, null, null, null, null, null);

        // Create a list of items to populate the adapter
        List<Item> itemList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Item item = getItemFromCursor(cursor);
                itemList.add(item);
            }
            cursor.close();
        }

        // Create and set the custom adapter
        adapter = new CustomAdapter(getActivity(), itemList);
        listView.setAdapter(adapter);

        return view;
    }

    // Helper method to create an Item object from the cursor
    private Item getItemFromCursor(Cursor cursor) {
        int columnCount = cursor.getColumnCount();
        StringBuilder itemText = new StringBuilder();
        
        // Iterate through all columns in the cursor
        for (int i = 0; i < columnCount; i++) {
            String columnName = cursor.getColumnName(i);
            String columnValue = cursor.getString(i);
            itemText.append(columnName).append(": ").append(columnValue).append("\n");
        }
        
        // Create an Item with the concatenated text
        return new Item(itemText.toString());
    }

}